# -*- coding: utf-8 -*-

import json
import logging
import time
from pathlib import Path

import arrow
import requests
from peewee import *
from tqdm import tqdm

LEAGUE = 'Synthesis'

# cache size 128 mb
sql_pragmas = {'cache_size': -1024 * 128}
listings_db = SqliteDatabase(Path('db/listings.db'), pragmas=sql_pragmas)

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


def timeit(f):
    """ Timing decorator."""

    def timed(*args, **kw):
        ts = arrow.now()
        result = f(*args, **kw)
        te = arrow.now()

        if (te - ts).total_seconds() < 60:
            total_time = f'{(te-ts).total_seconds():.2f}s'
        else:
            m, s = divmod((te - ts).total_seconds(), 60)
            total_time = f'{int(m)}m {int(s)}s'

        logging.info(f'function {f.__name__} took {total_time}')
        return result

    return timed


def load_json(filename: str) -> dict:
    with open(filename) as f:
        data = json.load(f)
    return data


class BaseModel(Model):
    class Meta:
        database = listings_db


class Result(BaseModel):
    """A raw result/response of the trade API request.
    One result represents one item listing.
    """

    listing_id = CharField(primary_key=True)
    listing_data = TextField()
    item_data = TextField()
    gone = BooleanField(default=False)


class Listing(BaseModel):
    """A parsed item listing with the price converted to chaos value
    according to the date the item was listed at.
    """

    listing_id = CharField(primary_key=True)
    mod_a = CharField(index=True)
    mod_b = CharField(index=True)
    mod_c = CharField(null=True, index=True)
    text_desc = TextField()
    indexed = DateTimeField()
    account = CharField()
    ilvl = IntegerField()
    original_price = CharField()
    price = FloatField()
    gone = BooleanField(default=False)


class Mods:
    """All possible aura mods for Watcher's Eye."""

    filename = Path('json/watchers_mods.json')

    def __init__(self):
        self.mods_grouped = load_json(self.filename)
        self.mods = self._ungroup_mods()
        self.groups = list(self.mods_grouped)
        self.short_names = self._get_short_names()

    def _ungroup_mods(self) -> dict:
        result = {}
        for k, v in self.mods_grouped.items():
            for k2, v2 in v.items():
                result[k2] = v2
        return result

    def _get_short_names(self) -> dict:
        res = {}
        for k, v in self.mods_grouped.items():
            for i, k2 in enumerate(v):
                res[k2] = f'{k} {i+1}'

        return res

    def __getitem__(self, item):
        return self.mods[item]

    def __contains__(self, item):
        return item in self.mods

    def __iter__(self):
        return iter(self.mods)

    def keys(self):
        return self.mods.keys()

    def values(self):
        return self.mods.values()

    def items(self):
        return self.mods.items()

    def get(self, item):
        return self.mods.get(item)

    def __len__(self):
        return len(self.mods)


class API:
    url = f'https://www.pathofexile.com/api/trade/search/{LEAGUE}'
    delay = float()
    big_timeout = float()
    timeout = 40
    comma_limit = 10

    @classmethod
    def find_rate_limit(cls) -> None:
        r = requests.get(cls.url)

        rate_limit = r.headers['X-Rate-Limit-Ip']
        request_limit, interval, timeout = map(int, rate_limit.split(':'))
        cls.delay = (interval / request_limit) * 1.15  # adding a 15% buffer to the delay
        cls.big_timeout = timeout + 1  # slight buffer here as well

        logging.info(f'Request limit: {request_limit}, interval: {interval}s, timeout: {timeout}s')
        logging.info(f'Chosen delay between requests: {cls.delay:.2f}s\n')
        cls.sleep_after_request(r)

    @staticmethod
    def construct_fetch_url(listings: list, query_id: str = '') -> str:
        url_start = 'https://www.pathofexile.com/api/trade/fetch/'
        url_middle = '?query=' if query_id else ''

        return f'{url_start}{",".join(listings)}{url_middle}{query_id}'

    @classmethod
    def make_request(cls, req: requests.Response) -> requests.Response:
        # make a throttled request
        max_attempts = 3
        response = None

        for _ in range(max_attempts):
            try:
                response = req
                response.raise_for_status()
                break
            except requests.exceptions.HTTPError as err:
                logging.warning(err)
                if response.status_code == requests.codes.too_many_requests:
                    time.sleep(cls.big_timeout)
            except requests.exceptions.ReadTimeout as err:
                logging.warning(err)
                time.sleep(cls.timeout)

        # something is not cool
        if response.status_code != requests.codes.ok:
            logging.error(f"Something is wrong, couldn't get a "
                          f"response after {max_attempts} attempts")
            if response.status_code:
                logging.error(f'Status code is {response.status_code}')
            logging.error('Aborting the program')
            raise RuntimeError

        # everything is cool
        cls.sleep_after_request(response)
        return response

    @classmethod
    def sleep_after_request(cls, response: requests.Response) -> None:
        if not cls.delay:
            cls.delay = 1.15
        elapsed = response.elapsed.total_seconds()
        duration = max(0, cls.delay - elapsed)
        time.sleep(duration)

    @staticmethod
    def get_all_currency_types():
        url = f'https://api.poe.watch/get?league={LEAGUE}&category=currency'
        r = requests.get(url)
        r.raise_for_status()
        return r.json()

    @staticmethod
    def get_one_currency_history(cur_id):
        url = f'https://api.poe.watch/item?id={cur_id}'
        r = requests.get(url)
        r.raise_for_status()
        API.sleep_after_request(r)
        return r.json()


class ListingManager:
    query_filename = Path('json/query.json')
    life_mana_es = ("explicit.stat_983749596", "explicit.stat_2482852589",
                    "explicit.stat_2748665614")

    def __init__(self):
        listings_db.connect(reuse_if_open=True)

    def __del__(self):
        listings_db.close()

    def update_old(self) -> None:
        if not API.delay or not API.big_timeout:
            API.find_rate_limit()

        total_gone = 0
        logging.info('Updating old listings...')

        # select listings that are not gone
        all_listings = Result.select(Result.listing_id).where(~Result.gone)
        trimmed_results = [x.listing_id for x in all_listings]
        for listings in tqdm(self._subqueries(trimmed_results), ncols=100):
            url = API.construct_fetch_url(listings)
            r = API.make_request(requests.get(url, timeout=API.timeout))

            subresults = r.json()['result']
            for subres in subresults:
                if subres:
                    listing_id = subres['id']
                    listing_data = json.dumps(subres['listing'])
                    item_data = json.dumps(subres['item'])
                    try:
                        # when a listing disappears, it usually stays in the api for some time
                        # with the price it was "gone" at
                        gone = subres['gone']
                        total_gone += 1
                    except KeyError:
                        gone = False
                    q = (Result
                         .update(listing_data=listing_data, item_data=item_data, gone=gone)
                         .where(Result.listing_id == listing_id))
                    q.execute()

                else:
                    # if result is None, that likely means the listing is long gone
                    id_ = subresults.index(subres)
                    listing_id = listings[id_]
                    gone = True
                    total_gone += 1
                    q = Result.update(gone=gone).where(Result.listing_id == listing_id)
                    q.execute()

        logging.info(f'Done. Listings gone: {total_gone}')

    def add_new(self) -> None:
        if not API.delay or not API.big_timeout:
            API.find_rate_limit()

        query = load_json(self.query_filename)
        total_new_listings = 0
        mods = Mods()

        logging.info('Looking for new listings...')

        for mod in tqdm(mods, ncols=100):
            query['query']['stats'][0]['filters'][0]['id'] = mod
            r = API.make_request(requests.post(API.url, json=query, timeout=API.timeout))

            response = r.json()
            new_listings = self._exclude_existing_listings(response["result"])
            if new_listings:
                for listings in self._subqueries(new_listings):
                    url = API.construct_fetch_url(listings, response['id'])
                    r2 = API.make_request(requests.get(url, timeout=API.timeout))

                    subresults = r2.json()['result']
                    for subres in subresults:
                        listing_id = subres['id']
                        listing_data = json.dumps(subres['listing'])
                        item_data = json.dumps(subres['item'])
                        try:
                            gone = subres['gone']
                        except KeyError:
                            gone = False

                        Result.create(
                            listing_id=listing_id,
                            listing_data=listing_data,
                            item_data=item_data,
                            gone=gone
                        )
                        total_new_listings += 1

        total_count = Result.select().count()

        logging.info(f'Done. New listings: {total_new_listings}')
        logging.info(f'Total raw listings: {total_count}')

    def display_listings_info(self):
        total_count = Listing.select().count()
        gone = Listing.select().where(Listing.gone).count()
        ungone = Listing.select().where(~Listing.gone).count()
        twomods = Listing.select().where(Listing.mod_c == '').count()
        threemods = Listing.select().where(Listing.mod_c != '').count()

        logging.info(f'Total listings: {total_count}')
        logging.info(f'Gone: {self._get_share(gone, total_count)} | '
                     f'Still available: {self._get_share(ungone, total_count)}')
        logging.info(f'2-mod jewels: {self._get_share(twomods, total_count)} | '
                     f'3-mod jewels: {self._get_share(threemods, total_count)}')

    @staticmethod
    def _get_share(count: int, total: int) -> str:
        return f'{count} ({count/total:.1%})'

    @staticmethod
    def _subqueries(all_res_ids: list) -> list:
        # split all listings into groups of 10, which is the limit of listings per request
        total = len(all_res_ids)
        n = API.comma_limit

        return [all_res_ids[i:i + n] for i in range(0, total, n)]

    @staticmethod
    def _exclude_existing_listings(listings: list) -> list:
        existing_listings = Result.select(Result.listing_id)
        result = set(listings)
        for row in existing_listings:
            if row.listing_id in result:
                result.remove(row.listing_id)
        return list(result)

    @staticmethod
    def no_listings() -> bool:
        total_count = Listing.select().count()
        return not total_count

    def parse_raw_results(self, use_existing_history: bool = False) -> None:
        cur_history = CurrencyHistory(use_existing=use_existing_history)
        all_mods = Mods()

        listings_db.drop_tables([Listing])
        listings_db.create_tables([Listing])

        logging.info('Parsing raw results...')

        with listings_db.atomic():
            for result in tqdm(Result.select(), ncols=100):
                i_data = json.loads(result.item_data)

                # if a jewel is corrupted into a rare, we skip it
                if i_data['name'] != "Watcher's Eye":
                    continue

                mods = []
                for mod in i_data['extended']['mods']['explicit']:
                    if mod['magnitudes'][0]['hash'] not in self.life_mana_es:
                        mods.append(mod['magnitudes'][0]['hash'])

                # 3-mod or 2-mod
                if len(mods) == 3:
                    mod_a, mod_b, mod_c = mods
                    text_desc = f'{all_mods[mod_a]}\n{all_mods[mod_b]}\n' \
                                f'{all_mods[mod_c]}'
                else:
                    mod_a, mod_b = mods
                    mod_c = ''
                    text_desc = f'{all_mods[mod_a]}\n{all_mods[mod_b]}'

                l_data = json.loads(result.listing_data)
                indexed = arrow.get(l_data['indexed']).naive
                account = l_data['account']['name']
                if l_data['price']:
                    amount = l_data['price']['amount']
                    cur = l_data['price']['currency']
                    orig_price = f'{amount} {cur}'
                    price = cur_history.get_chaos_equiv(amount, cur, l_data['indexed'])
                else:
                    orig_price, price = 0, 0

                if price:
                    Listing.create(
                        listing_id=result.listing_id,
                        mod_a=mod_a,
                        mod_b=mod_b,
                        mod_c=mod_c,
                        text_desc=text_desc,
                        indexed=indexed,
                        account=account,
                        ilvl=i_data['ilvl'],
                        original_price=orig_price,
                        price=price,
                        gone=result.gone
                    )

        self.display_listings_info()

    def update_everything(self, only_add_new=False):
        if not only_add_new:
            self.update_old()
        self.add_new()
        self.parse_raw_results()

    @staticmethod
    def show_item_info(listing_id: str):
        mods = Mods()
        r = Listing.get_by_id(listing_id)
        indexed = arrow.get(r.indexed)
        print(f'Listing {listing_id}')
        print('Mods:')
        print(f'- {mods[r.mod_a]}')
        print(f'- {mods[r.mod_b]}')
        print(f'- {mods[r.mod_c]}')
        print(f'Indexed: {r.indexed} ({indexed.humanize()})')
        print(f'Account name: {r.account}')
        print(f'Original price: {r.original_price}')
        print(f'Chaos equiv price: {r.price}')
        if r.gone:
            print(f'Gone: yes')


class CurrencyHistory:
    """Creates a json file that contains currency rates for each day of the current league
    for all the currency types that Watcher's Eye jewels were listed at.
    """
    filename = Path('json/currency_history.json')
    currency_abbr = Path('json/currency_abbr.json')

    def __init__(self, use_existing: bool = False):
        if use_existing:
            self.history = load_json(self.filename)
        else:
            logging.info('Updating currency history...')
            self.league = LEAGUE

            self._curs_idless = self.find_all_used_currency()
            self._curs = self.get_ids_for_currency()
            self.history = self.get_history_for_one_currency()

            with open(self.filename, 'w') as f:
                json.dump(self.history, f)

    def get_history_for_one_currency(self) -> dict:
        history = {}

        for cur_id, v in tqdm(self._curs.items(), ncols=100):
            cur = API.get_one_currency_history(cur_id)

            parsed_cur = {'name': cur['name']}
            for data in cur['data']:
                if data['league']['name'] == self.league:
                    parsed_cur['mean'] = data['mean']
                    parsed_cur['history'] = []
                    for day in data['history']:
                        parsed_cur['history'].append({'time': day['time'], 'mean': day['mean']})

            history[v] = parsed_cur

        return history

    def find_all_used_currency(self) -> dict:
        abbrs = load_json(self.currency_abbr)

        existing_listings = Result.select(Result.listing_data)
        all_diff_cur = set()
        for l in existing_listings:
            x = json.loads(l.listing_data)
            if x['price']:
                all_diff_cur.add(x['price']['currency'])

        return {x: abbrs[x] for x in all_diff_cur}

    def get_ids_for_currency(self) -> dict:
        curs_with_id = {}
        for cur in API.get_all_currency_types():
            for k, v in self._curs_idless.items():
                if cur['name'] == v:
                    curs_with_id[cur['id']] = k

        return curs_with_id

    def get_chaos_equiv(self, amount: float, cur_type: str, date: str) -> float:
        if cur_type == 'chaos':
            return round(amount, 2)

        try:
            cur = self.history[cur_type]
        except KeyError:
            logging.warning(f'no history for this currency! {cur_type}')
            return 0

        rate = float()
        for day in cur['history']:
            if date[:10] == day['time'][:10]:  # stripping to date
                rate = day['mean']

        # if there's no such day in history, we take the latest mean rate
        if not rate:
            rate = cur['mean']

        return round(amount * rate, 2)
