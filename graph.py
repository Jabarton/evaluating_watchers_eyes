import json

from main import timeit, SingleMod, DoubleMod, Mods


def generate_colors(l: int) -> list:
    start = 0x474747
    end = 0xC8C8C8
    step = (end - start) // l
    colors_list = [f'#{i :0>6x}' for i in range(start, end, step)]
    return colors_list


def get_avg_of_two_colors(c1: str, c2: str) -> str:
    size = 2
    length = 6
    base = 16

    c1, c2 = c1.strip('#'), c2.strip('#')
    c1_parts = [c1[i:i + size] for i in range(0, length, size)]
    c2_parts = [c2[i:i + size] for i in range(0, length, size)]
    r, g, b = [(int(a, base) + int(b, base)) // 2 for a, b in zip(c1_parts, c2_parts)]

    return f'#{r :0>2x}{g :0>2x}{b :0>2x}'


@timeit
def generate_json_for_graph():
    data = {
        'nodes': [],
        'links': []
    }

    threshold = 900

    mods_medians = {}
    for r in SingleMod.select():
        mods_medians[r.mods] = r.median

    mods_grouped = Mods().mods_grouped
    colors = generate_colors(len(mods_grouped))
    for key, group in mods_grouped.items():
        color = colors.pop()
        for i, mod in enumerate(group):
            price = mods_medians[mod]
            desc = f'{group[mod]}\nMedian price: {price}c'
            mod_dict = {
                'desc': desc,
                'name': f'{key} {i + 1}',
                'id': mod,
                'price': price,
                'color': color,
                'group': key
            }
            data['nodes'].append(mod_dict)

    for r in DoubleMod.select():
        link_dict = {}
        mod_a, mod_b = r.mods.split(',')

        c1, c2 = None, None
        for mod in data['nodes']:
            if mod['id'] == mod_a:
                c1 = mod['color']
            if mod['id'] == mod_b:
                c2 = mod['color']
        color = get_avg_of_two_colors(c1, c2)

        mod_a_median = mods_medians[mod_a]
        mod_b_median = mods_medians[mod_b]
        ratio = r.median / max(mod_a_median, mod_b_median)

        link_dict['price'] = r.median
        link_dict['ratio'] = ratio
        link_dict['source'] = mod_a
        link_dict['target'] = mod_b
        link_dict['color'] = color

        # setting threshold condition for links
        if link_dict['price'] > threshold:
            data['links'].append(link_dict)

    with open('graph\price_data1.json', 'w') as f:
        json.dump(data, f)


if __name__ == '__main__':
    generate_json_for_graph()
