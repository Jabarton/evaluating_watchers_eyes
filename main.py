# -*- coding: utf-8 -*-

from listings import ListingManager
from stats import StatMaker


def update_listings_and_stats():
    lm = ListingManager()
    lm.update_everything(only_add_new=False)
    sm = StatMaker()
    sm.store_stats_in_db()
    sm.save_listings_dataframe()


def add_new_listings():
    lm = ListingManager()
    lm.add_new()


def make_stats():
    sm = StatMaker()
    sm.store_stats_in_db()
    sm.save_listings_dataframe()


if __name__ == '__main__':
    update_listings_and_stats()
    # add_new_listings()
    # make_stats()
