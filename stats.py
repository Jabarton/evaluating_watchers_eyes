# -*- coding: utf-8 -*-

import itertools
from collections import defaultdict
from pathlib import Path

import numpy as np
import pandas as pd
from peewee import *
from scipy import stats as scis

from listings import Mods, ListingManager, sql_pragmas, timeit, Listing

stats_db = SqliteDatabase(Path('db/stats.db'), pragmas=sql_pragmas)


class Stat(Model):
    mods = CharField(unique=True)
    mod_desc = TextField()
    amount = IntegerField()
    median = FloatField()
    mean = FloatField()
    mode = FloatField(default=0)
    pstdev = FloatField()
    min_ = FloatField()
    max_ = FloatField()

    class Meta:
        database = stats_db
        legacy_table_names = False


class SingleMod(Stat):
    pass


class SingleModGone(Stat):
    pass


class DoubleMod(Stat):
    pass


class StatMaker:
    mods = Mods()
    cutoff_price = 100_000  # if an item costs more than that, don't include it in stat tables

    def __init__(self):
        listing_manager = ListingManager()
        if listing_manager.no_listings():
            listing_manager.parse_raw_results(use_existing_history=True)
        stats_db.connect(reuse_if_open=True)

    def __del__(self):
        stats_db.close()

    def save_listings_dataframe(self):
        df_list = []
        for r in Listing.select():
            df_list.append({'mod_a': self.mods.short_names.get(r.mod_a),
                            'mod_b': self.mods.short_names.get(r.mod_b),
                            'mod_c': self.mods.short_names.get(r.mod_c),
                            'date': r.indexed,
                            'ilvl': r.ilvl,
                            'price': r.price,
                            'gone': r.gone
                            })

        df = pd.DataFrame(df_list)
        lower_limit = np.percentile(df.price, 0.1)
        upper_limit = np.percentile(df.price, 99.9)
        df = df[(lower_limit < df.price) & (df.price < upper_limit)]
        df = df.set_index('date')

        df.to_parquet('listings.parquet')

    @timeit
    def store_stats_in_db(self):
        self.analyze_single_mods()
        self.analyze_single_mods_gone_only()
        self.analyze_double_mods()

    def analyze_single_mods(self) -> None:
        stats_db.drop_tables([SingleMod])
        stats_db.create_tables([SingleMod])

        with stats_db.atomic():
            for mod in self.mods:
                result = Listing.select(Listing.price).where(
                    ((Listing.mod_a == mod) | (Listing.mod_b == mod) | (Listing.mod_c == mod)) &
                    (Listing.price < self.cutoff_price))

                stats = self.get_stats_from_prices(np.array([r.price for r in result]))
                SingleMod.create(mods=mod, mod_desc=self.mods[mod], **stats)

    def analyze_single_mods_gone_only(self) -> None:
        stats_db.drop_tables([SingleModGone])
        stats_db.create_tables([SingleModGone])

        with stats_db.atomic():
            for mod in self.mods:
                result = Listing.select(Listing.price).where(
                    ((Listing.mod_a == mod) | (Listing.mod_b == mod) | (Listing.mod_c == mod)) &
                    Listing.gone &
                    (Listing.price < self.cutoff_price))

                stats = self.get_stats_from_prices(np.array([r.price for r in result]))
                SingleModGone.create(mods=mod, mod_desc=self.mods[mod], **stats)

    def analyze_double_mods(self) -> None:
        stats_db.drop_tables([DoubleMod])
        stats_db.create_tables([DoubleMod])

        combo_prices = defaultdict(list)
        result = Listing.select().where(Listing.price < self.cutoff_price)
        for r in result:
            for pair in itertools.combinations([x for x in (r.mod_a, r.mod_b, r.mod_c) if x], 2):
                combo_prices[frozenset(pair)].append(r.price)

        with stats_db.atomic():
            for combo, prices in combo_prices.items():
                stats = self.get_stats_from_prices(np.array(prices), trim=False)
                DoubleMod.create(mods=','.join(combo),
                                 mod_desc='\n'.join([self.mods[x] for x in combo]),
                                 **stats)

    @staticmethod
    def get_stats_from_prices(prices: np.ndarray, trim: bool = True) -> dict:
        if trim:
            prices = scis.trimboth(prices, 0.05)

        stats = {'mean': np.mean(prices),
                 'median': np.median(prices),
                 'pstdev': np.std(prices),
                 'min_': np.amin(prices),
                 'max_': np.amax(prices),
                 'amount': len(prices),
                 'mode': scis.mode(prices)[0][0]}

        return {k: np.around(v, 1) for k, v in stats.items()}


if __name__ == '__main__':
    sm = StatMaker()
    # sm.make_all_stats()
    sm.save_listings_dataframe()
